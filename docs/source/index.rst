.. i-Melt documentation master file, created by
   sphinx-quickstart on Thu Nov  4 09:09:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to i-Melt's documentation!
==================================

Copyright (2021) C. Le Losq and co.

Charles Le Losq, Institut de physique du globe de Paris, University of Paris
lelosq@ipgp.fr

Andrew P. Valentine, University of Durham
andrew.valentine@durham.ac.uk

The i-Melt project
------------------

i-Melt is a physics-guided neural network model, that combines deep neural networks with physical equations to predict the structural, thermodynamic and dynamic properties of aluminosilicate melts and glasses.

The project is hosted on `Github <https://github.com/charlesll/i-melt>`_, a `Streamlit web calculator <https://share.streamlit.io/charlesll/i-melt/imelt_streamlit.py>`_ is available and you can read the paper explaining the model `here <https://doi.org/10.1016/j.gca.2021.08.023>`_!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   data
   training
   result_analysis
   predictions
   references

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
