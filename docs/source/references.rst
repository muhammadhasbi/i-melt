References
==========

`Le Losq C., Valentine A. P., Mysen B. O., Neuville D. R., 2021. Structure and properties of alkali aluminosilicate glasses and melts: insights from deep learning. Geochimica and Cosmochimica Acta, https://doi.org/10.1016/j.gca.2021.08.023 <https://doi.org/10.1016/j.gca.2021.08.023>`_

`Le Losq C., Valentine A. P., 2021. charlesll/i-melt: i-Melt v1.2.1 (v1.2.1). Zenodo. https://doi.org/10.5281/zenodo.5342178 <https://doi.org/10.5281/zenodo.5342178>`_
