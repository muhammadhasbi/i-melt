Result analysis
---------------

To repeat the analysis of the results reported in `Le Losq et al. GCA 2021 <https://doi.org/10.1016/j.gca.2021.08.023>`_, three notebooks are provided:

* `Results_experiments.ipynb <https://github.com/charlesll/i-melt/blob/master/Results_experiments.ipynb>`_ allows observing the results of the random search and dataset size experiments in Supplementary Figure 1 (see ./figures folder). *Please note that the originally trained networks for experiments 1 and 2 take too much size and are thus not provided via this repository. Running this notebook thus requires to first run the codes Experiment_1_architecture.py AND Experiment_2_dataset_size.py (see above)*

* `Results_model_performance.ipynb <https://github.com/charlesll/i-melt/blob/master/Results_model_performance.ipynb>`_ makes a statistical analysis of the performance of the bagged 10 best models.

* `Results_predictions.ipynb <https://github.com/charlesll/i-melt/blob/master/Results_predictions.ipynb>`_ allows generating all the other figures and the analysis presented in the paper.

All figures are saved in `i-melt/figures <https://github.com/charlesll/i-melt/tree/master/figures>`_
