Database
========

Database localisation
---------------------

All data are given in a `Database_IPGP.xlsx <https://github.com/charlesll/i-melt/blob/master/data/Database_IPGP.xlsx>`_ file in the `data folder <https://github.com/charlesll/i-melt/tree/master/data>`_. Raman spectra are contained in the `i-melt/data/raman/ <https://github.com/charlesll/i-melt/tree/master/data/raman>`_ folder.

Data preparation
----------------

The notebook `Dataset_preparation.ipynb <https://github.com/charlesll/i-melt/blob/master/Dataset_preparation.ipynb>`_ allows preparation of the datasets, which are subsequently saved in HDF5 format in the data folder.

The `Dataset_visualization.ipynb <https://github.com/charlesll/i-melt/blob/master/Dataset_visualization_Fig1.ipynb>`_ notebook shows the distribution of data in ternary plots.
