# i-Melt

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5342178.svg)](https://doi.org/10.5281/zenodo.5342178)![GitHub](https://img.shields.io/github/license/charlesll/i-melt)

i-Melt is a physics-guided neural network model, that combines deep neural networks with physical equations to predict the structural, thermodynamic and dynamic properties of aluminosilicate melts and glasses.

- **Web app** https://share.streamlit.io/charlesll/i-melt/imelt_streamlit.py
- **Documentation (in progress)** https://i-melt.readthedocs.io/en/latest/
- **Source code:** https://github.com/charlesll/i-melt/tree/master/i-melt/imelt.py
- **License:** https://github.com/charlesll/i-melt/LICENSE
- **Bug reports:** https://github.com/charlesll/i-melt/issues
- **Contact** lelosq@ipgp.fr

Please see the original publication Le Losq et al. (2021) for details: https://www.sciencedirect.com/science/article/abs/pii/S0016703721005007

## Software Contributors

Charles Le Losq, University of Paris, Institut de physique du globe de Paris, lelosq@ipgp.fr

Andrew P. Valentine, Durham University, andrew.valentine@durham.ac.uk
